module.exports = {
    semi: true,
    singleQuote: true,
    quoteProps: 'as-needed',
    jsxSingleQuote: false,
    bracketSpacing: true,
    jsxBracketSameLine: true,
    arrowParens: 'avoid',
    endOfLine: 'lf',
};
